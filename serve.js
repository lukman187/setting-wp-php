/**
 * Spawn a PHP Dev server instance.
 */

const PHPServer = require('php-server-manager');
const conf = require('./config.js');

const server = new PHPServer({
	host: '127.0.0.1',
	script: 'router.php',
	port: conf.server.port,
	directory: conf.server.root,
    directives: {
        display_errors: 0,
        expose_php: 0
    }
});

server.run();
