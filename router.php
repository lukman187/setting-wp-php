<?php
/**
 * Simple router file to handle WordPress requests.
 */

$uri = urldecode(
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

if ($uri !== '/' && file_exists('/Users/tj/Projects/WPThemeLabs/wp-themes-dev/webroot'.$uri)) {
    return false;
}
require_once '/Users/tj/Projects/WPThemeLabs/wp-themes-dev/webroot/index.php';
