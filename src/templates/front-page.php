<?php get_header();?>
	<main class="container">
		<div class="row">
			<div class="col text-center py-5">
				<i class="fas fa-home fa-4x"></i>
				<h1>
					Front page.
				</h1>

				<h2>
					Theme's Color.
				</h2>
				<div class="box-color mx-auto my-4">
					<div class="bg-primary py-4">
						Primary
					</div>
					<div class="bg-secondary py-4 text-white">
						Secondary
					</div>
					<div class="bg-success py-4">
						Success
					</div>
					<div class="bg-info py-4">
						Info
					</div>
					<div class="bg-warning py-4">
						Warning
					</div>
					<div class="bg-danger py-4 text-white">
						Danger
					</div>
				</div>

			</div>
		</div>
	</main>
<?php get_footer(); ?>
